import React, { Component } from "react";
import todosList from "./todos.json";
import { v4 as uuidv4 } from 'uuid';
import TodoItem from './components/TodoItem/TodoItem';
import TodoList from './components/TodoList/TodoList'
import Navigation from "./components/Navigation/Navigation";

class App extends Component {
  state = {
    todos: todosList,
    // count: 0,
  };

  handleDelete =(itemId)=>{ 
    const newitems = this.state.todos.filter(
     item =>item.id !==itemId
    )
    this.setState({ todos: newitems })
    console.log('isDeleted');
  }
// add new items
  handleNewTodo=(event)=>{
    // event.preventDefault()
    const newTodo = {
      userId: 1,
      id: uuidv4(),
      title: event.target.value,
      completed: false,
        }
    if(event.key === 'Enter'){
      console.log(newTodo);
      console.log(true);

      this.setState({todos: [...this.state.todos, newTodo]})
      event.target.value=''
        
    // console.log(true);
    //     event.preventDefault()
    }


  }
 handleDeleteAll=(eventnp)=>{

 }
  render() {
    return (
      <section className="todoapp">
        
        <header className="header">
          <h1>todos</h1>
         
          <input className="new-todo" onKeyPress={this.handleNewTodo} placeholder="What needs to be done?" value={this.state.input} autoFocus />
        </header>
        <TodoList todos={this.state.todos} deleteTodo={this.handleDelete}/>
        <footer className="footer">
        {/* <!-- This should be `0 items left` by default --> */}
        <span className="todo-count">
          <strong>0</strong> item(s) left
        </span>
        <Navigation />
                 {/* <ul className="filters">
        <li>
          <a href="/">All</a>
        </li>
        <li>
          <a href="/active">Active</a>
        </li>
        <li>
          <a href="/completed">Completed</a>
        </li>
        </ul> */}
          <button className="clear-completed" onClick={this.clear}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

// class TodoItem extends Component {
//   state = {
//     items: '',
//     completed: true,
//     name: 'completed',
//     check: false,
//   }
// //delete stff
 
// // completed stuff

  
//   changeClassName = event =>{
//     this.setState(state => ({
//         name: !state.name,
//         check: !state.check
//       }));
    
//     }

    
//   render() {
//     return (
//       <li className={this.state.name ? "" : "completed"} onClick={this.changeClassName} >                  
//         <div className="view">  
//           <input className="toggle" type="checkbox" checked={this.state.check ? true : false} />
//           <label>{this.props.title}</label>
//           <button className="destroy" onClick={(event)=>this.props.deleteTodo(this.props.id)} />
//         </div>
//       </li>
//     );
//   }
// }

// class TodoList extends Component {
  
//   render() {
//     return (
//       <section className="main" >
//         <ul className="todo-list" >
//           {this.props.todos.map((todo) => (
//             <TodoItem title={todo.title} completed={todo.completed} key={todo.id} id={todo.id} deleteTodo={this.props.deleteTodo}/>
//           ))}
//         </ul>
//       </section>
//     );
//   }
// }

export default App;

// delete function

// class DeleteTodo extends Component {
//   state = {
//     accounts: [{ }],
//   }

//   handleDelete1 = accountId => event => {
//     const newAccounts = App.state.todos.filter(
//       account => account.id !== accountId
//     )
//     this.setState({ accounts: newAccounts })
//   }

//   render () {
//     return (
//       <React.Fragment>
//         <h1>Active Accounts</h1>
//         {this.state.accounts.map(account => (
//           <div>
//             <p>Account: {account.id}</p>
//             <button onClick={this.handleDelete1(account.id)}>
//               Delete Account
//             </button>
//           </div>
//         ))}
//       </React.Fragment>
//     )
//   }
// }