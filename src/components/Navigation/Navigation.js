import React from 'react'
import { Link } from "react-router-dom";
import Active from './../Active/Active'
function Navigation(props) {
  return (
         <ul className="filters">
        <li>
          {/* <a href="/">All</a> */}
          <Link to= '/'>All</Link>
        </li>
        <li>
          <a href='/Active'>Active</a>
        </li>
        <li>
          <a href="/completed">Completed</a>
        </li>
        </ul> 
  );
}
export default Navigation;


