// const { Component } = require("react")

import React, { Component } from 'react'

class TodoItem extends Component {
    state = {
      items: '',
      completed: true,
      name: 'completed',
      check: false,
    }
  //delete stff
   
  // completed stuff
  
    
    changeClassName = event =>{
      this.setState(state => ({
          name: !state.name,
          check: !state.check
        }));
      
      }
  
      
    render() {
      return (
        <li className={this.state.name ? "" : "completed"} onClick={this.changeClassName} >                  
          <div className="view">  
            <input className="toggle" type="checkbox" checked={this.state.check ? true : false} />
            <label>{this.props.title}</label>
            <button className="destroy" onClick={(event)=>this.props.deleteTodo(this.props.id)} />
          </div>
        </li>
      );
    }
  }

  export default TodoItem