import React, { Component } from 'react'
import TodoItem from './../TodoItem/TodoItem'

class TodoList extends Component {
  
    render() {
      return (
        <section className="main" >
          <ul className="todo-list" >
            {this.props.todos.map((todo) => (
              <TodoItem title={todo.title} completed={todo.completed} key={todo.id} id={todo.id} deleteTodo={this.props.deleteTodo}/>
            ))}
          </ul>
        </section>
      );
    }
  }
  
  export default TodoList